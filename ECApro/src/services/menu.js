import { api, headers } from "./api";
const URL = `/api/login`;
const header = { headers: headers };

function getOpciones(id) {
  const path = `${URL}/permisos/${id}`;
  return api.get(path, header).then(res => res.data);
}

export { getOpciones };
